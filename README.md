# livesetsus-dl

![](https://img.shields.io/badge/written%20in-PHP-blue)

A downloader for livesets.us.

## Usage


```
Usage:
  livesetsus-dl [args]

Commands:
  --help                      Display this message
  -o %option %value           Set an option. See below for details
  --recent                    Add recent livesets to working results
  --search %string            Search for livesets by DJ, and add to working
                                results
  --filter %string            Filter current working results by %string
  --url-info                  Display info URLs for each working result
  --url-audio                 Display audio URLs for each working result

Options:
  destdir                     Download destination directory. Default cwd
  getinfo                     Whether to save metadata text file. Default false
  getaudio                    Whether to save audio file. Default true
  skipifexist                 Skip saving file if already exists. Default true
  maxsearchresults            Maximum results for '--search'. Default 20 (sets)
  maxrecentpages              Maximum pages for '--recent'. Default 2 (days)

Examples:

  - Download last 2 days of added sets  
      livesetsus-dl --recent
      
  - Download last 40 sets from Armin Van Buuren including metadata
      livesetsus-dl \ 
        -o getinfo true \ 
	-o maxsearchresults 40 \ 
	--search 'Armin Van Buuren'

```



## Download

- [⬇️ livesetsus-dl_r01.tar.gz](dist-archive/livesetsus-dl_r01.tar.gz) *(3.09 KiB)*
